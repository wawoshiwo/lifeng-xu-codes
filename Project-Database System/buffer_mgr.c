#ifndef BUFFER_MANAGER_C
#define BUFFER_MANAGER_C

#include<stdio.h>
#include<stdlib.h>
#include "buffer_mgr.h"
#include "storage_mgr.h"
#include <math.h>
int size,read,write;

RC FIFO(BM_BufferPool *const bm,BM_FrameHandle *const fh){
    
    //printf("fh is : %d\n",fh->frame->pageNum);
    
    if(bm->mgmtData==NULL){
        //printf("FIFO-First\n");
        fh->previous=fh;
        bm->mgmtData=fh;
        size++;
        
       // if(bm->mgmtData!=NULL)
        //    printf("BM is NOT EMPTy\n");
       // else
         //   printf("BM is EMPTy\n");
        return RC_OK;
    }
    
    BM_FrameHandle *head= (BM_FrameHandle *)bm->mgmtData;
    //printf("Head is : %d\n",head->frame->pageNum);
    BM_FrameHandle *tail= head->previous;
    
    if(size<bm->numPages){
       // printf("FIFO-Middle\n");
        
        tail->next=fh;
        fh->previous=tail;
        tail=fh;
        
        head->previous=tail;
        size++;
        return RC_OK;
    }
    
    if(size>=bm->numPages){
       //printf("FIFO-Overflow\n");
      //  printf("Head is : %d\n",head->frame->pageNum);
        
        if(head->fixed==0){
           if(head->dirty==1)
              forcePage(bm,head->frame);
           bm->mgmtData=head->next;
        }
        else{
            int i;
            for(i=0;i<bm->numPages;i++){
                head=head->next;
                //printf("FIFO-head->next:%D\n",head->frame->pageNum);
                if(head!=NULL && head->fixed==0){

                    if(head->dirty==1)
                      forcePage(bm,head->frame);
                    head->previous->next=head->next;
                    if(head->next!=NULL)
                      head->next->previous=head->previous;
                    break;
                }
            }
            if(head==NULL){
                printf("Please close some clients to spare new buffer frames!");
                return RC_BUFFER_HAS_NO_AVAILABLE_FRAME;
            }
        }
        // printf("Head is changed into : %d\n",head->frame->pageNum);
         free(head->frame->data);
         head->frame->data=NULL;
         free(head->frame);
         head->frame=NULL;
         free(head);
          head=NULL;
        
        
       head=(BM_FrameHandle *)bm->mgmtData;
        
       size--;
        
       tail->next=fh;
       fh->previous=tail;
       tail=fh;
        
       head->previous=tail;
       size++;
        
       return RC_OK;
    }
    
     return RC_OK;
        
}
RC LRU(BM_BufferPool *const bm, BM_FrameHandle *const fh){
    
    if(bm->mgmtData==NULL){
        fh->previous=fh;
        bm->mgmtData=fh;
        size++;
        return RC_OK;
    }
    
    BM_FrameHandle *head= (BM_FrameHandle *)bm->mgmtData;
    BM_FrameHandle *tail= head->previous;
    
    if(size<bm->numPages){
        fh->next=head;
        head->previous=fh;
        head=fh;
        head->previous=tail;
        size++;
        bm->mgmtData=head;
        tail->next=NULL;
        return RC_OK;
    }
    
    BM_FrameHandle *newTail= tail->previous;
    if(size==bm->numPages){
        if(tail->fixed==0){
            head->previous=newTail;
            newTail->next=NULL;
            if(tail->dirty==1){
                forcePage(bm,head->frame);
                tail->previous->next=NULL;
            }
        }
        else{
            int i;
            for(i=1;i<bm->numPages;i++){
                tail=tail->previous;
                printf("LRU-tail->previous:%D\n",tail->frame->pageNum);
                if(tail->previous->next!=NULL && tail->fixed==0){
                    if(head->dirty==1)
                        forcePage(bm,head->frame);
                    tail->previous->next=tail->next;
                    tail->next->previous=tail->previous;
                    break;
                }
                
                else if(tail->fixed==0){
                    bm->mgmtData=tail->next;
                    head=tail->next;
                    tail->next->previous=newTail->next;
                    break;
                }
                else if(tail->previous->next==NULL && tail->fixed==1){
                    printf("Please close some clients to spare new buffer frames!");
                    return RC_BUFFER_HAS_NO_AVAILABLE_FRAME;
                }
             }
            
        }

        
        free(tail->frame->data);
        tail->frame->data=NULL;
        free(tail->frame);
        tail->frame=NULL;
        free(tail);
        tail=NULL;
        
        tail=newTail;
        size--;
        
        head->previous=fh;
        fh->next=head;
        head=fh;
        head->previous=tail;
        size++;
        bm->mgmtData=head;
        tail->next=NULL;
        return RC_OK;
    }
    
    return RC_OK;
}

RC LRU_M(BM_BufferPool *const bm, BM_FrameHandle *fh){//used to maintain LRU
    BM_FrameHandle *head= (BM_FrameHandle *)bm->mgmtData;
    BM_FrameHandle *tail= head->previous;
    
    if(fh->previous->next!=NULL && fh->next!=NULL){
        //printf("Changing head into %d\n",fh->frame->pageNum);
        fh->previous->next=fh->next;
        fh->next->previous=fh->previous;
        fh->next=head;
        head->previous=fh;
        fh->previous=tail;
        bm->mgmtData=fh;
        head=fh;
    }
    else if(fh->previous->next!=NULL && fh->next==NULL){
        //printf("Changing head into %d\n",fh->frame->pageNum);
        tail=fh->previous;
        tail->next=NULL;
        fh->next=head;
        head->previous=fh;
        bm->mgmtData=fh;
        head=fh;
    }
        
    return RC_OK;
}

RC getFrameHandle(BM_BufferPool *const bm,BM_FrameHandle *fh,BM_PageHandle *const page){
    if(bm->mgmtData!=NULL){
        fh=(BM_FrameHandle *)bm->mgmtData;
        while(fh!=NULL){
            if(fh->frame->pageNum==page->pageNum)
                return RC_OK;
            fh=fh->next;
        }//end while
    }

    return RC_OK;
    
}
RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName,
                  const int numPages, ReplacementStrategy strategy,
                  void *stratData){
    bm->pageFile=(char *)pageFileName;
    bm->numPages=numPages;
    bm->strategy=strategy;
    bm->mgmtData=NULL;
    size=0;
    read=0;
    write=0;
      //printf("Initialize\n");
    return RC_OK;
}
RC shutdownBufferPool(BM_BufferPool *const bm)
{
    forceFlushPool(bm);
    if(bm->mgmtData!=NULL){
      BM_FrameHandle *fh=NULL;
      BM_FrameHandle *pre=NULL;
      fh=(BM_FrameHandle *)bm->mgmtData;
      bm-> mgmtData=NULL;
      while(fh!=NULL){
         free(fh->frame->data);
         fh->frame->data=NULL;
         free(fh->frame);
         fh->frame=NULL;
         pre=fh;
         fh=fh->next;
         free(pre);
         pre=NULL;
       }
     }
    bm->pageFile=NULL;
    bm->numPages=0;
    bm->strategy=(ReplacementStrategy)NULL;
    bm->mgmtData=NULL;
    // printf("Shutdown\n");
    return RC_OK;
}
RC forceFlushPool(BM_BufferPool *const bm){
   
    if(bm->mgmtData==NULL){
        printf("buffer is emtpty");
        return RC_OK;
    }
    BM_FrameHandle *fh=NULL;
    fh=(BM_FrameHandle *)bm->mgmtData;
    while(fh!=NULL){
        forcePage(bm,fh->frame);
        fh=fh->next;
    }
    return RC_OK;
}

// Buffer Manager Interface Access Pages
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page){
    // printf("Begin Markdirty\n");
    BM_FrameHandle *fh=NULL;
    
    if(bm->mgmtData!=NULL){
        fh=(BM_FrameHandle *)bm->mgmtData;
        while(fh!=NULL){
            if(fh->frame->pageNum==page->pageNum){
                //printf("Markdirty success\n");
                fh->dirty=1;
                return RC_OK;
            }
            fh=fh->next;
        }//end while
    }
   return RC_ERROR1;
    
}
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
   // printf("Begin Unpin\n");
    
    BM_FrameHandle *fh=NULL;
    if(bm->mgmtData!=NULL){
        fh=(BM_FrameHandle *)bm->mgmtData;
        while(fh!=NULL){
            if(fh->frame->pageNum==page->pageNum){
                 //printf("page %d Before:%D\n",fh->frame->pageNum,fh->fixed);
                fh->fixed-=1;
               // printf("after:%D\n",fh->fixed);
                //printf("Unpin Success\n");
                return RC_OK;
            }
            fh=fh->next;
        }//end while
    }

    return RC_ERROR2;
}
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page){
    BM_FrameHandle *fh=NULL;
    
    if(bm->mgmtData!=NULL){
        fh=(BM_FrameHandle *)bm->mgmtData;
        while(fh!=NULL){
            if(fh->frame->pageNum==page->pageNum){
                //printf("Begin to write page %d:%s\n",page->pageNum,page->data);
                break;
            }
            fh=fh->next;
        }//end while
    }
    if(fh==NULL)
        return RC_ERROR3;

    if(fh->fixed>0){
        printf("Page %d is still being occupied by some clients fixed %d. Please unpin it\n",page->pageNum,fh->fixed);
        return RC_ERROR;
    }
    
    if(fh->dirty==0)
        return RC_ERROR;
    
    SM_FileHandle *smfh= (SM_FileHandle *)malloc(sizeof(SM_FileHandle));
    openPageFile(bm->pageFile,smfh);
     // printf("Begin to write page %d:%s\n",page->pageNum,page->data);
    writeBlock(page->pageNum,smfh,(SM_PageHandle)page->data);
    
    fh->dirty=0;
    
    free(smfh);
    smfh=NULL;
    write++;
    return RC_OK;
}
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page,
            const PageNumber pageNum){
    // printf("Begin pin\n");

    page->pageNum=pageNum;
    
    BM_FrameHandle *fh=NULL;
    //getFrameHandle(bm,fh,page);
    if(bm->mgmtData!=NULL){
       // printf("Searching Buffer\n");
        fh=(BM_FrameHandle *)bm->mgmtData;
        
        while(fh!=NULL){
            //printf("Searching frame: %d \n",fh->frame->pageNum);
            if(fh->frame->pageNum==pageNum){
               // printf("Hit!!!!!\n");
                fh->fixed+=1;
                page->data=fh->frame->data;
                if(bm->strategy==1)
                    LRU_M(bm,fh);
                return RC_OK;
            }
            fh=fh->next;
        }//end while
    }
    //write somethin into DummyPageFile
   // char *expected = malloc(sizeof(char) * 512);
   // printf("No epxceted page\n");
    //page not in pool
    fh=(BM_FrameHandle *)calloc(sizeof(BM_FrameHandle),1);
    fh->frame=(BM_PageHandle *)calloc(sizeof(BM_PageHandle),1);
    fh->frame->data=(char *)calloc(PAGE_SIZE,1);
    //sprintf(page->data, "%s-%i", "Page", page->pageNum);
    
    SM_FileHandle *smfh= (SM_FileHandle *)malloc(sizeof(SM_FileHandle));
    openPageFile(bm->pageFile,smfh);
    
    //printf("Reading from file\n");
    readBlock(pageNum,smfh,(SM_PageHandle)fh->frame->data);
    
    fh->frame->pageNum=pageNum;
    
    page->data=fh->frame->data;
    fh->fixed=1;
    fh->dirty=0;
    fh->previous=NULL;
    fh->next=NULL;
    
    if(bm->strategy==0)
    {
       // printf("Push %d\n",fh->frame->pageNum);
        FIFO(bm,fh);
        //printf("Pushed %d\n",fh->frame->pageNum);
        
    }
    
    if(bm->strategy==1)
         LRU(bm,fh);
    
   // if(bm->mgmtData==NULL)
        //printf("Pin failed\n");
   // else
       // printf("Pin success\n");
    
    //if(bm->mgmtData==NULL)
      //  printf("bm is Empty still\n");
    
    read++;
    
    free(smfh);
    smfh=NULL;
    
    //printf("fixed:%d\n",fh->fixed);
    return RC_OK;
}

// Statistics Interface
PageNumber *getFrameContents (BM_BufferPool *const bm){
    PageNumber *frames = malloc(sizeof(PageNumber)*bm->numPages);
    int j;
    for(j=bm->numPages-1;j>=size;j--)
        frames[j]=NO_PAGE;
    
    BM_FrameHandle *curFrame ;
    if(bm->mgmtData==NULL)
        curFrame=NULL;
    else
        curFrame = (BM_FrameHandle *)bm->mgmtData;
    int i;
    for(i=0;i<size;i++){
        frames[i]=curFrame->frame->pageNum;
        curFrame=curFrame->next;
    }
    
    return frames;
}
bool *getDirtyFlags (BM_BufferPool *const bm){
    bool *frames = malloc(sizeof(PageNumber)*bm->numPages);
    int j;
    for(j=bm->numPages-1;j>=size;j--)
        frames[j]=false;
    BM_FrameHandle *curFrame ;
    if(bm->mgmtData==NULL)
        curFrame=NULL;
    else
        curFrame = (BM_FrameHandle *)bm->mgmtData;
    int i;
    for(i=0;i<size;i++){
        frames[i]=curFrame->dirty;
        curFrame=curFrame->next;
    }
    
    return frames;
}
int *getFixCounts (BM_BufferPool *const bm){
    int *frames = malloc(sizeof(PageNumber)*bm->numPages);
    int j;
    for(j=bm->numPages-1;j>=size;j--)
        frames[j]=0;
    
    BM_FrameHandle *curFrame ;
    if(bm->mgmtData==NULL)
        curFrame=NULL;
    else
        curFrame = (BM_FrameHandle *)bm->mgmtData;
    int i;
    for(i=0;i<size;i++){
        frames[i]=curFrame->fixed;
        curFrame=curFrame->next;
    }
    
    return frames;
}
int getNumReadIO (BM_BufferPool *const bm){
    return read;
}
int getNumWriteIO (BM_BufferPool *const bm){
    return write;
}


#endif
