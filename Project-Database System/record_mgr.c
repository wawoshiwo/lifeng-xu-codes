#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "record_mgr.h"
#include "buffer_mgr.h"
#include "storage_mgr.h"

typedef struct RM_ScanInfo
{
    Expr *cond;
    int numPages;
    int numTuples;
    int curPageNum;
    int curSlot;
    int count;
} RM_ScanInfo;

// table and manager
BM_BufferPool *bm;
SM_FileHandle *fh;
ReplacementStrategy strategy;
BM_PageHandle *bh;
BM_PageHandle *headerPage;
BM_PageHandle *schemaPage;


int bpSize;
int attrSize;
int schemaPageNum;
int headerPageNum;
int numTuples;
int freePages;

extern RC initRecordManager (void *mgmtData){
    
    bm=(BM_BufferPool *)calloc(1,sizeof(BM_BufferPool));
    bh=(BM_PageHandle *)calloc(1,sizeof(BM_PageHandle));
    fh=(SM_FileHandle *)calloc(1,sizeof(SM_FileHandle));
 
    headerPage=(BM_PageHandle *)calloc(1,sizeof(BM_PageHandle));
    schemaPage=(BM_PageHandle *)calloc(1,sizeof(BM_PageHandle));
    strategy=0;
    bpSize=5;
    schemaPageNum=0;
    headerPageNum=1;
    attrSize=2;
    return RC_OK;
}
extern RC shutdownRecordManager (){

    free(headerPage);
    free(schemaPage);
    free(bm);
    free(bh);
    free(fh);
    bm=NULL;
    return RC_OK;
}
extern RC createTable (char *name, Schema *schema){
    initStorageManager();
    createPageFile(name);
    openPageFile(name,fh);
    appendEmptyBlock(fh);
    appendEmptyBlock(fh);
    closePageFile(fh);
    
    initBufferPool(bm,name,bpSize,strategy,NULL);
    
    pinPage(bm,bh,schemaPageNum);
    
   // printf("Create schema:\n");
   // printf("types:%d,%d,%d\n\n",schema->dataTypes[0],schema->dataTypes[1],schema->dataTypes[2]);
    
    int curPo=0;
    
    *(int *)&bh->data[curPo]=schema->numAttr;
     //printf("numAttr:%d\n",*(int *)&bh->data[curPo]);
    
    curPo+=sizeof(int);
    
    int i;
    for( i=0; i<schema->numAttr;i++){
        strncpy(&bh->data[curPo],schema->attrNames[i],attrSize);
        
       //  printf("Attrname%d:%s\n",i,&bh->data[curPo]);
        
        curPo+=attrSize;
    }
    
    for( i=0; i<schema->numAttr;i++){
      *(int *)&bh->data[curPo+sizeof(int)*i]=schema->dataTypes[i];
        
    }
     //printf("datatypes:%d %d %d\n",*(int *)&bh->data[curPo],*(int *)&bh->data[curPo+4],*(int *)&bh->data[curPo+8]);
    
    curPo+=sizeof(int)*schema->numAttr;
    
    for( i=0; i<schema->numAttr;i++){
        *(int *)&bh->data[curPo+sizeof(int)*i]=schema->typeLength[i];
        
    }
    //printf("typesLength:%d %d %d\n",*(int *)&bh->data[curPo],*(int *)&bh->data[curPo+4],*(int *)&bh->data[curPo+8]);
    curPo+=schema->numAttr*sizeof(int);
    
    *(int *)&bh->data[curPo]=schema->keySize;
     // printf("keySize:%d\n",*(int *)&bh->data[curPo]);
    curPo+=sizeof(int);
   
    strncpy(&bh->data[curPo],(char *)schema->keyAttrs,sizeof(int)*schema->keySize);
    //printf("typesLength:%d %d %d\n",*(int *)&bh->data[curPo],*(int *)&bh->data[curPo+4],*(int *)&bh->data[curPo+8]);
   
    
    markDirty(bm,bh);
    unpinPage(bm,bh);
    
    pinPage(bm,bh,headerPageNum);
    
    
    int recsPerPage=
         (PAGE_SIZE-2*sizeof(int))/(sizeof(RID)+sizeof(int)+getRecordSize(schema));
    int numTuples=0;
    int freePages=0;

    *(int *)bh->data=recsPerPage;
    *(int *)&bh->data[sizeof(int)]=numTuples;// the size of the table( how many records int it)
    *(int *)&bh->data[2*sizeof(int)]=freePages;//the number of pages having free space (0) means no free space
   // printf("recsPerPage: %d\n", *(int *)&bh->data[0]);
   // printf("numTuples: %d\n", *(int *)&bh->data[sizeof(int)]);
   // printf("freePage: %d\n", *(int *)&bh->data[2*sizeof(int)]);
    
    markDirty(bm,bh);
    unpinPage(bm,bh);
     //printf("******************record-mgr-testpoint\n");
   shutdownBufferPool(bm);
    
    return RC_OK;
}
extern RC openTable (RM_TableData *rel, char *name){
    
    initStorageManager();
    openPageFile(name,fh);

    initBufferPool(bm,name,bpSize,strategy,NULL);
    pinPage(bm,bh,schemaPageNum);

    rel->name=name;
    
    rel->schema=(Schema *)calloc(1,sizeof(Schema));
    
    int curPo=0;
   
    
    rel->schema->numAttr=*(int *)&bh->data[curPo];
    
    rel->schema->attrNames=(char **)calloc(1,rel->schema->numAttr*sizeof(char *));
    
    curPo+=sizeof(rel->schema->numAttr);
    
    int i;
    for( i=0;i<rel->schema->numAttr;i++){
      rel->schema->attrNames[i]=&bh->data[curPo];
      curPo+=attrSize;
    }
    
    rel->schema->dataTypes=(DataType *)&bh->data[curPo];
    curPo+=sizeof(int)*rel->schema->numAttr;
    
    rel->schema->typeLength=(int *)&bh->data[curPo];
    curPo+=rel->schema->numAttr*sizeof(int);
    
    rel->schema->keySize=*(int *)&bh->data[curPo];
    curPo+=sizeof(int);
    
    rel->schema->keyAttrs=(int *)&bh->data[curPo];
    
    pinPage(bm,headerPage,headerPageNum);
    
   // printf("Recs: %d\n",*(int *)&headerPage->data[sizeof(int)]);
    

    return RC_OK;
    
}
extern RC closeTable (RM_TableData *rel){
    free(rel->schema->attrNames);
    free(rel->schema);
    unpinPage(bm,headerPage);
    unpinPage(bm,schemaPage);
    shutdownBufferPool(bm);
    return RC_OK;
}
extern RC deleteTable (char *name){
    destroyPageFile(name);
    return RC_OK;
}
extern int getNumTuples (RM_TableData *rel){
    return *(int *)&headerPage->data[sizeof(int)];
}

// handling records in a table
extern RC insertRecord (RM_TableData *rel, Record *record){
    
    int slotSize=sizeof(RID)+getRecordSize(rel->schema);

    //strncpy(&temp[2*sizeof(int)],record->data,slotSize-2*sizeof(int));
    
    int *numTuples=(int *)&headerPage->data[sizeof(int)];
    int *freePages=(int *)&headerPage->data[2*sizeof(int)];
    
   // printf("Freepages:%d\n",*freePages);
    
    
    if(*freePages==0){
        appendEmptyBlock(fh);
       
        pinPage(bm,bh,fh->totalNumPages-1);
         //printf("Insert into new page %d\n",bh->pageNum);
        *freePages=1;
        *(int *)&headerPage->data[3*sizeof(int)]=fh->totalNumPages-1;
        markDirty(bm,headerPage);
        
        *(int *)&bh->data[0]=0; //number of records in the page is 0
        *(int *)&bh->data[sizeof(int)]=0;//number of deleted records in the page is 0
    }
    else
        pinPage(bm,bh,*(int *)&headerPage->data[2*sizeof(int)+*freePages*sizeof(int)]);
    
    record->id.page=bh->pageNum;
    
    int *numRecords=(int *)&bh->data[0];
    int *deletedRecNum=(int *)&bh->data[sizeof(int)];
    
    int offset;
    if(*deletedRecNum==0){
        offset=slotSize*(*numRecords+1);
        record->id.slot=*numRecords+1;
        *numRecords+=1;
    }
    else{
        int *slotNum=(int *)&bh->data[(1+*deletedRecNum)*sizeof(int)];
        offset=*slotNum*slotSize;
        record->id.slot=*slotNum;
        *deletedRecNum-=1;
    }
    
    *(int *)&bh->data[offset]=record->id.page;
    *(int *)&bh->data[offset+sizeof(int)]=record->id.slot;
    *(int *)&bh->data[offset+2*sizeof(int)]=*(int *)record->data;
    strncpy(&bh->data[offset+3*sizeof(int)],&record->data[sizeof(int)],4);
    *(int *)&bh->data[offset+4*sizeof(int)]=*(int *)&record->data[8];//*(int *)&record->data[3*sizeof(int)]+rel->schema->typeLength[1];
    
    *numTuples+=1;
   
    int *recordPerPage=(int *)&headerPage->data[0];
    
    if(*numRecords==*recordPerPage && *deletedRecNum==0)
        *freePages-=1;
    /**
    printf("a to be inserted: %d\n",*(int *)record->data);
    printf("b to be inserted: %s\n",&record->data[4]);
    printf("c to be inserted: %d\n",*(int *)&record->data[8]);
    
    printf("Insert into page %d\n",bh->pageNum);
    printf("numRecords: %d\n",*(int *)&bh->data[0]);
    printf("deletedRecords: %d\n",*(int *)&bh->data[4]);
    printf("page: %d\n",*(int *)&bh->data[offset]);
     printf("slot: %d\n",*(int *)&bh->data[offset+4]);
     printf("a: %d\n",*(int *)&bh->data[offset+8]);
     printf("b: %s\n",&bh->data[offset+12]);
     printf("c: %d\n",*(int *)&bh->data[offset+16]);
    printf("\n*******************\n");
    **/
    markDirty(bm,bh);
    unpinPage(bm,bh);
    
    return RC_OK;
    
        
}
extern RC deleteRecord (RM_TableData *rel, RID id){
    int slotSize=sizeof(RID)+getRecordSize(rel->schema);
    int recordPerPage=*(int *)&headerPage->data[0];
    pinPage(bm,bh,id.page);

    int *numRecords=(int *)&bh->data[0];
    int *deletedRecNum=(int *)&bh->data[sizeof(int)];
    int *deletedSlot =(int *)&bh->data[(2+*deletedRecNum)*sizeof(int)];
    
    if(*numRecords==recordPerPage && deletedRecNum==0){
        int *freePages=(int *)&headerPage->data[2*sizeof(int)];
        int *newFreePage=(int *)&headerPage->data[(*freePages+3)*sizeof(int)];
        *newFreePage=id.page;
        *freePages+=1;
        markDirty(bm,headerPage);
    }
    
    *(int *)&bh->data[PAGE_SIZE-id.slot * slotSize]=-1; //put a tombstone to delete the record
    *deletedSlot=id.slot;
    *deletedRecNum+=1;
    
    markDirty(bm,bh);
    unpinPage(bm,bh);
    
    return RC_OK;
}
extern RC updateRecord (RM_TableData *rel, Record *record){
    
    pinPage(bm,bh,record->id.page);
    int slotSize=sizeof(RID)+getRecordSize(rel->schema);
    
    int offset=record->id.slot*slotSize+2*sizeof(int);
    
    *(int *)&bh->data[offset]=*(int *)&record->data[0];
    strncpy(&bh->data[offset+sizeof(int)],&record->data[sizeof(int)],sizeof(int));
    *(int *)&bh->data[offset+2*sizeof(int)]=*(int *)&record->data[2*sizeof(int)];
    
    markDirty(bm,bh);
    unpinPage(bm,bh);
    
    return RC_OK;
}
extern RC getRecord (RM_TableData *rel, RID id, Record *record){
    
    int slotSize=sizeof(RID)+getRecordSize(rel->schema);
    
    pinPage(bm,bh,id.page);
    
    /**
    if(id.page >= fh->totalNumPages)
         return RC_READ_NON_EXISTING_RECORD;
    
    int *page=(int *)&bh->data[PAGE_SIZE - id.slot * slotSize];
    
    int *savedRec=(int *)bh->data;
    
    if( *page==0 ||  id.slot>*savedRec) //tombstone or empty record
        return RC_READ_NON_EXISTING_RECORD;
    
    //char *recordData=&bh->data[PAGE_SIZE-id.slot*slotSize+2*sizeof(int)];
    **/
    
    int offset=slotSize*id.slot;
    
    record->id.page=id.page;
    record->id.slot=id.slot;
     //printf("SLOTSIZE:%d  OFFSET: %d  SLOT: %d \n",slotSize,offset,id.slot);
     //printf("datasize:%d  ridsize: %lu \n",getRecordSize(rel->schema),sizeof(RID));

    //*(int *)&bh->data[offset]=record->id.page;
    //*(int *)&bh->data[offset+sizeof(int)]=record->id.slot;
    
   
    *(int *)record->data=*(int *)&bh->data[offset+2*sizeof(int)];
    
    
    
    strncpy(&record->data[sizeof(int)],&bh->data[offset+3*sizeof(int)],4);
    *(int *)&record->data[8]=*(int *)&bh->data[offset+4*sizeof(int)];
    

   // printf("pinned data: %s\n",bh->data);//[PAGE_SIZE-id.slot*slotSize+2*sizeof(int)]
   // strncpy(record->data,&bh->data[PAGE_SIZE-id.slot*slotSize+2*sizeof(int)],slotSize-2*sizeof(int));
    /**
    printf("a to be inserted: %d\n",*(int *)record->data);
    printf("b to be inserted: %s\n",&record->data[4]);
    printf("c to be inserted: %d\n",*(int *)&record->data[8]);
    
    printf("Insert into page %d\n",bh->pageNum);
    printf("numRecords: %d\n",*(int *)&bh->data[0]);
    printf("deletedRecords: %d\n",*(int *)&bh->data[4]);
    printf("page: %d\n",*(int *)&bh->data[offset]);
    printf("slot: %d\n",*(int *)&bh->data[offset+4]);
    printf("a: %d\n",*(int *)&bh->data[offset+8]);
    printf("b: %s\n",&bh->data[offset+12]);
    printf("c: %d\n",*(int *)&bh->data[offset+16]);
    **/
    unpinPage(bm,bh);
    return RC_OK;
}

// scans
extern RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond){
   
    RM_ScanInfo * scanInfo=(RM_ScanInfo *)calloc(1,sizeof(RM_ScanInfo));
    
    scan->rel=rel;
    scanInfo->cond=cond;
    scanInfo->numPages=fh->totalNumPages;
    scanInfo->numTuples=*(int *)&headerPage->data[sizeof(int)];
    scanInfo->curPageNum=headerPageNum+1;
    scanInfo->curSlot=0;
    scanInfo->count=0;
    
    scan->mgmtData=scanInfo;
    return RC_OK;
    
}

extern RC next (RM_ScanHandle *scan, Record *record){
    
    RM_ScanInfo *scanInfo=(RM_ScanInfo *)scan->mgmtData;
    
    if(scanInfo->numTuples==0 )
        return RC_RM_NO_MORE_TUPLES;
    
    RID *id=(RID *)calloc(1,sizeof(RID));
    
    Value **result=(Value **)calloc(1,sizeof(Value *));
    Value *res=NULL;

   do{
       if(res!=NULL)
          free(res);
    id->page=scanInfo->curPageNum;
    id->slot=scanInfo->curSlot+=1;
        
       // printf("!!!!!scan page %d, slot %d \n",id->page,id->slot);
       
    
    getRecord(scan->rel,*id,record);
       /*
        printf("Begin next\n");
        printf("page: %d\n",*(int *)&record->id.page);
        printf("slot: %d\n",*(int *)&record->id.slot);
        printf("a: %d\n",*(int *)&record->data[0]);
        printf("b: %s\n",&record->data[4]);
        printf("c: %d\n",*(int *)&record->data[8]);*/
    
    evalExpr(record,scan->rel->schema,scanInfo->cond,result);
        //printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    scanInfo->count+=1;
    
    res=*result;
     
   
    }while(res->v.boolV==false && scanInfo->count<= scanInfo->numTuples);
    
      free(res);
    free(result);
    free(id);
    
    if( scanInfo->count>scanInfo->numTuples)
        return RC_RM_NO_MORE_TUPLES;
    
    return RC_OK;
    
}
extern RC closeScan (RM_ScanHandle *scan){
    free(scan->mgmtData);
    return RC_OK;
}

// dealing with schemas
extern int getRecordSize (Schema *schema){
    int recSize=0;
    int i;
    for(i=0;i<schema->numAttr;i++){
        if(schema->typeLength[i]!=0){
            recSize+=schema->typeLength[i];
            //printf("typelenght %d: %d\n",i,schema->typeLength[i]);
        }
        else
            recSize+=sizeof(int);
    }
    
    return recSize;
}
extern Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys){
    Schema *schema = (Schema *)malloc(sizeof(Schema)); // allocate memory to schema
    schema->numAttr = numAttr; // set Number of Attribute
    schema->attrNames = attrNames; // set Attribute Names
    schema->dataTypes =dataTypes; // set  Attribute's data types
   // printf("Create schema:\n");
   // printf("types:%d,%d,%d\n\n",schema->dataTypes[0],schema->dataTypes[1],schema->dataTypes[2]);
    
    schema->typeLength = typeLength; // set Attribute's type length
    schema->keySize = keySize;  // set Key size
    schema->keyAttrs = keys; // set key attribute
    
    return schema;
    
}
extern RC freeSchema (Schema *schema){
    free(schema);
    return RC_OK;
}

// dealing with records and attribute values
extern RC createRecord (Record **record, Schema *schema){
    
    Record *newRecord=(Record *)calloc(1,sizeof(Record));
    
    int recSize = getRecordSize(schema);
 
    newRecord->data=(char *)calloc(1,recSize);
    
    *record=newRecord;
    
    return RC_OK;
}
extern RC freeRecord (Record *record){
    free(record);
    return RC_OK;
}
extern RC getAttr (Record *record, Schema *schema, int attrNum, Value **value){
    
    DataType dataType=schema->dataTypes[attrNum];
    
    int typeLength=schema->typeLength[attrNum];
    if(typeLength==0)
        typeLength=sizeof(int);
    
    Value *result=(Value *)calloc(1,sizeof(Value));
    
    *value=result;

    result->dt=dataType;
    
    int dataPo=0;
    int i;
    for(i=0;i<attrNum;i++){
     if(schema->typeLength[attrNum]==0)
        dataPo+=4;
     else
        dataPo+=schema->typeLength[attrNum];
    }
    
    switch(dataType){
        case DT_INT :
            result->v.intV=*(int *)&record->data[dataPo];
            break;
        case DT_STRING:
            result->v.stringV=(char *)calloc(1,typeLength+1);
            strncpy(result->v.stringV,&record->data[dataPo],typeLength);
            break;
        case DT_FLOAT:
            result->v.floatV=*(float *)&record->data[dataPo];
        
            break;
        case DT_BOOL:
             result->v.boolV=*(bool *)&record->data[dataPo];
            break;
    }
    
    return RC_OK;
    
}
extern RC setAttr (Record *record, Schema *schema, int attrNum, Value *value){
   
    int dataType=value->dt;
    
    int typeLength=schema->typeLength[attrNum];
    if(typeLength==0)
        typeLength=sizeof(int);
    
    int dataPo=0;
    int i;
    for(i=1;i<=attrNum;i++)
        if(schema->typeLength[i-1]==0)
           dataPo+=sizeof(int);
        else
           dataPo+=schema->typeLength[i-1];

    
    switch(dataType){
        case DT_INT :
            *(int *)&record->data[dataPo]=value->v.intV;
            break;
        case DT_STRING :
            strncpy(&record->data[dataPo],value->v.stringV,typeLength);
            break;
        case DT_FLOAT :
            *(float *)&record->data[dataPo]=value->v.floatV;
            break;
        case DT_BOOL :
            *(bool *)&record->data[dataPo]=value->v.boolV;
            break;
    }
    
    
    return RC_OK;
}
