//
//  storage_mgt.c
//  Storage_mgt
//
//  Created by LIFENG XU on 9/6/15.
//  Copyright (c) 2015 XU. All rights reserved.
//

//
//  storage_mgt.c
//  databse_mgt
//
//  Created by LIFENG XU on 9/6/15.
//
//


#ifndef databse_mgt_storage_mgt_c
#define databse_mgt_storage_mgt_c

#include<stdio.h>
#include<stdlib.h>
#include "storage_mgr.h"
#include <math.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "dberror.h"

FILE *filePointer;

void initStorageManager (void){
    filePointer=NULL;
}

RC createPageFile(char *fileName){
    filePointer=fopen(fileName,"w+");
    
    if(filePointer==NULL)
        return RC_FILE_NOT_FOUND;
    /**
    else{
        SM_PageHandle EmptyPage = (SM_PageHandle) calloc(PAGE_SIZE,sizeof(char));
        if(fwrite(EmptyPage,sizeof(char),PAGE_SIZE,filePointer)<PAGE_SIZE)
            printf("File written wrong\n");
        free(EmptyPage);

        EmptyPage=NULL;
        fclose(filePointer);
        return RC_OK;
    }
    **/
    return RC_OK;
}

RC openPageFile (char *fileName, SM_FileHandle * fHandle){
    
    //printf("Open file:");
    //printf(fileName);
  //  printf("\n");
    
    filePointer = fopen(fileName, "r+");
    
    if(filePointer == NULL)
        return RC_FILE_NOT_FOUND;
  
        fHandle->fileName = fileName;
        fHandle->curPagePos = 0;
        struct stat fStatus;
       if (fstat(fileno(filePointer), &fStatus)< 0)
           printf("File written wrong\n");
    
       //  printf("file size: %d\n",fStatus.st_size);
        fHandle->totalNumPages = fStatus.st_size/PAGE_SIZE;
        
        fclose(filePointer);
  //  printf("Close file^^^^^^^^^^^^^^^^^\n");
        return RC_OK;
        
    
}


RC closePageFile (SM_FileHandle *fHandle){
    
    if(filePointer!=NULL)
        filePointer=NULL;
    return RC_OK;
        
}

RC destroyPageFile(char *fileName){
    filePointer=fopen(fileName,"r");
    
    if(filePointer==NULL)
        return RC_FILE_NOT_FOUND;
    fclose(filePointer);
    
    remove(fileName);
    
    return RC_OK;
}

RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
    filePointer = fopen(fHandle->fileName, "r");
    if(filePointer == NULL){
        return RC_FILE_NOT_FOUND;
    }
    if(pageNum > fHandle->totalNumPages || pageNum <0){
        fclose(filePointer);
        return RC_READ_NON_EXISTING_PAGE;

    }
    
    fseek(filePointer, (pageNum * PAGE_SIZE), SEEK_SET);
    
    if (fread(memPage,1,PAGE_SIZE,filePointer) == PAGE_SIZE)
       // printf("Read\n");
    
    fHandle->curPagePos = ftell(filePointer)/PAGE_SIZE;
    fclose(filePointer);
    return RC_OK;


}

int getBolockPos(SM_FileHandle *fHandle){
    
    return fHandle->curPagePos;
}

RC readFirstBlock(SM_FileHandle *fHandle,SM_PageHandle memPage){
    
    filePointer=fopen(fHandle->fileName,"r");
    
    if(filePointer==NULL)
        return RC_FILE_NOT_FOUND;
    
    for(int i=0;i<PAGE_SIZE;i++)
        if(feof(filePointer))
            break;
        else
            memPage[i]=fgetc(filePointer);
    
    fHandle->curPagePos=ftell(filePointer)/PAGE_SIZE;
    fclose(filePointer);
    return RC_OK;
    
}

RC readPreviousBlock(SM_FileHandle *fHandle,SM_PageHandle memPage){
    filePointer=fopen(fHandle->fileName,"r+");
    
    if(fHandle->curPagePos==0)
        return RC_READ_NON_EXISTING_PAGE;
    
    fseek(filePointer,(fHandle->curPagePos-1)*PAGE_SIZE,SEEK_SET);
    fHandle->curPagePos--;
    
    for(int i=0;i<PAGE_SIZE;i++)
        memPage[i]=fgetc(filePointer);
    
    fclose(filePointer);
    return RC_OK;
    
}

RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    filePointer=fopen(fHandle->fileName,"r+");
    
    if(filePointer==NULL)
        return RC_FILE_NOT_FOUND;
    
    int start=(fHandle->totalNumPages-1)*PAGE_SIZE;
    
    fseek(filePointer,start,SEEK_SET);
    
    for(int i=0;i<PAGE_SIZE;i++){
        char c =fgetc(filePointer);
        if(feof(filePointer))
            break;
        memPage[i]=c;
    }
    
    fHandle->curPagePos=fHandle->totalNumPages;
    fclose(filePointer);
    
    return RC_OK;
}

extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    int start=(PAGE_SIZE*(fHandle->curPagePos-1));
    
    filePointer=fopen(fHandle->fileName,"r");
    if(filePointer==NULL)
    {
        return RC_TEST;
    }
    fseek(filePointer,start,SEEK_SET);
    for(int i=0;i<PAGE_SIZE;i++)
    {
        char c = fgetc(filePointer);
        if(feof(filePointer))
            break;
        memPage[i] = c;
    }
    fclose(filePointer);
    return RC_OK;
    
}

extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
    if(fHandle->curPagePos==fHandle->totalNumPages)
        return RC_READ_NON_EXISTING_PAGE;
    
    int start=(PAGE_SIZE*fHandle->curPagePos);
    
    filePointer=fopen(fHandle->fileName,"r");
    
    if(filePointer==NULL)
        return RC_FILE_NOT_FOUND;
    
    
    fseek(filePointer,start,SEEK_SET);
    
    for(int i=0;i<PAGE_SIZE;i++)
    {
        char c = fgetc(filePointer);
        if(feof(filePointer))
            break;
        memPage[i] = c;
    }
    fclose(filePointer);
    return RC_OK;
    
}

extern RC writeBlock (int pageNum, SM_FileHandle *const fHandle, SM_PageHandle const memPage){
   // printf("Writting...\n");
    fHandle->curPagePos=pageNum;
    writeCurrentBlock(fHandle,memPage);
    return RC_OK;
}



//write block to current Page

extern RC writeCurrentBlock (SM_FileHandle *const fHandle, SM_PageHandle const memPage)
{
    //open the file in read and write mode
    filePointer = fopen(fHandle->fileName, "r+");
   // printf("Openning file: %s\n",fHandle->fileName);
    //check the existence of the file
    if(filePointer == NULL)
        return RC_FILE_NOT_FOUND;
    
    //Appending page to add new content
    
    fseek(filePointer,fHandle->curPagePos*PAGE_SIZE,SEEK_SET);
    //printf("Current Position: %d\n",ftell(filePointer));
    
    if(feof(filePointer))
      appendEmptyBlock(fHandle);
    
    fwrite(memPage,1,PAGE_SIZE,filePointer);
    
    fclose(filePointer);
    
    return RC_OK;
    
}



extern RC appendEmptyBlock (SM_FileHandle *fHandle){
    //printf("Begin append\n");
    
    filePointer = fopen(fHandle->fileName, "r+");
    //check the existence of the file
    if(filePointer == NULL)
        return RC_FILE_NOT_FOUND;
    /**
    SM_PageHandle EmptyPage=(SM_PageHandle)calloc(PAGE_SIZE,1);

    fseek(filePointer,0,SEEK_END);
    
    int w= fwrite(EmptyPage,1,PAGE_SIZE,filePointer) ;
    
    if(w==PAGE_SIZE)
        printf("write success\n");
    
    else
       printf("write failed\n");
    **/

    fHandle->totalNumPages++;
   // printf("Complete append");
  //  free(EmptyPage);
   // EmptyPage=NULL;
    fclose(filePointer);
    return RC_OK;
    }
                                                     
                                                     
extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle)
    {
        filePointer = fopen(fHandle->fileName, "a");
        if(filePointer == NULL)
            return RC_FILE_NOT_FOUND;
        while(numberOfPages > fHandle->totalNumPages)
            appendEmptyBlock(fHandle);
        
        fclose(filePointer);
        return RC_OK;
    }



#endif
