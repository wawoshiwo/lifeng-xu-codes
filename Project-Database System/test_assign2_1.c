  #include "storage_mgr.h"
#include "buffer_mgr_stat.h"
#include "buffer_mgr.h"
#include "dberror.h"
#include "test_helper.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// var to store the current test's name
char *testName;

// check whether two the content of a buffer pool is the same as an expected content 
// (given in the format produced by sprintPoolContent)
#define ASSERT_EQUALS_POOL(expected,bm,message)			        \
  do {									\
    char *real;								\
    char *_exp = (char *) (expected);                                   \
    real = sprintPoolContent(bm);					\
    if (strcmp((_exp),real) != 0)					\
      {									\
	printf("[%s-%s-L%i-%s] FAILED: expected <%s> but was <%s>: %s\n",TEST_INFO, _exp, real, message); \
	free(real);							\
	exit(1);							\
      }									\
    printf("[%s-%s-L%i-%s] OK: expected <%s> and was <%s>: %s\n",TEST_INFO, _exp, real, message); \
    free(real);								\
  } while(0)

// test and helper methods
static void testCreatingAndReadingDummyPages (void);
static void createDummyPages(BM_BufferPool *bm, int num);
static void checkDummyPages(BM_BufferPool *bm, int num);

static void testReadPage (void);

static void testFIFO (void);
static void testLRU (void);
static void user_test(void);

// main method
int 
main (void) 
{
  initStorageManager();
  testName = "";

  testCreatingAndReadingDummyPages();
  testReadPage();
  testFIFO();
  testLRU();
 user_test();
}

void
user_test(){
    BM_BufferPool *bm = MAKE_POOL();
    BM_PageHandle *h = MAKE_PAGE_HANDLE();
    
    testName = "user_test";
    
    CHECK(createPageFile("testbuffer.bin"));
    
    createDummyPages(bm, 10000);
    int numPages;
    int order;
    
    printf("^-^OK!Now you can try to build your own customized buffer pool!\n*****************\n");
    printf("Please input the buffer pool's size(Number of Frames):");
    scanf("%d",&numPages);
    do{
      printf("And the buffer order(0-FIFO,1-LRU):");
      scanf("%d",&order);
    }while(order<0 || order>1);
    
    
    CHECK(initBufferPool(bm, "testbuffer.bin", numPages, order, NULL));
    
    printf("@-@: Your buffer manger is ready.The pagefile has 10000 dummy pages with content [Page-(pageNum)].\n");
    
    int pinPageNum;
    int edit;
    int force;
    char *content = (char *)malloc(PAGE_SIZE);
    BM_FrameHandle *fh;
    
    for(;;){
        
       printf("\nWhich page to pin[-1 to restart the buffer manager,below -1 to quit]:");
       scanf("%d",&pinPageNum);
        
        if(pinPageNum<-1){
            printf("Wrong Page Number or quit command------Quit!\n");
            break;
        }
        
        if(pinPageNum==-1)
        {
              printf("Shutting down the buffer manager....\n");
              CHECK(shutdownBufferPool(bm));
              printf("Restarting the buffer manager...\n");
              CHECK(initBufferPool(bm, "testbuffer.bin", numPages, order, NULL));
            printf("@-@: Your buffer manger is ready.");
        }
        else{
           CHECK(pinPage(bm,h,pinPageNum));
        
           printf("The content in page %d :\n*************\n %s\n**************\n",h->pageNum,h->data);
            
            fh=(BM_FrameHandle *)bm->mgmtData;
            printf("Pool Frames:");
            while(fh!=NULL){
                printf(" [%d] ",fh->frame->pageNum);
                fh=fh->next;
            }
            
             printf("\n");
           printf("Do you want to replace the content? (1 for yes, others for no) :");
           scanf("%[^\n]d",&edit);
            
           

           if(edit==1){
              printf("Please enter the content you want to store in page %d (%d bytes most):",h->pageNum,PAGE_SIZE);
               scanf("%[^\n]",content);
               //fgets(content,4096,stdin);
             // scanf("%[^\n]s",content);
              //printf("%s\n",content);
              sprintf(h->data,"%s",content);
              markDirty(bm,h);
              printf("[[[[[[[Page-%d]]]]]] is changed, you can restart the buffer manager by pinning -1 to write it into the file.\n",h->pageNum);
              //printf("HDATA: %s\n",h->data);
          }
            CHECK(unpinPage(bm,h));
        }
        
    }
    
    CHECK(shutdownBufferPool(bm));
    CHECK(destroyPageFile("testbuffer.bin"));
    
    free(content);
    content=NULL;
    free(bm);
    bm=NULL;
    free(h);
    h=NULL;
    TEST_DONE();
    
}
// create n pages with content "Page X" and read them back to check whether the content is right
void
testCreatingAndReadingDummyPages (void)
{
  BM_BufferPool *bm = MAKE_POOL();
  testName = "Creating and Reading Back Dummy Pages";

  CHECK(createPageFile("testbuffer.bin"));

  createDummyPages(bm,22);
  checkDummyPages(bm, 20);

  createDummyPages(bm, 10000);
  checkDummyPages(bm, 10000);

  CHECK(destroyPageFile("testbuffer.bin"));

  free(bm);
  TEST_DONE();
}


void 
createDummyPages(BM_BufferPool *bm, int num)
{
  int i;
  CHECK(initBufferPool(bm, "testbuffer.bin", 3, RS_FIFO, NULL));
    
  BM_PageHandle *h = MAKE_PAGE_HANDLE();
  SM_FileHandle *smfh=(SM_FileHandle *)malloc(sizeof(SM_FileHandle));
  CHECK(openPageFile(bm->pageFile,smfh));

  
  
  for (i = 0; i < num; i++)
    {
      CHECK(appendEmptyBlock(smfh));
      CHECK(pinPage(bm, h, i));
      sprintf(h->data, "%s-%i", "Page", h->pageNum);
      CHECK(markDirty(bm, h));
      CHECK(unpinPage(bm,h));
    }

  CHECK(shutdownBufferPool(bm));
    
  free(h);
  free(smfh);
  
}

void 
checkDummyPages(BM_BufferPool *bm, int num)
{
  int i;
  BM_PageHandle *h = MAKE_PAGE_HANDLE();
  char *expected = malloc(sizeof(char) * 512);

  CHECK(initBufferPool(bm, "testbuffer.bin", 3, RS_FIFO, NULL));

  for (i = 0; i < num; i++)
    {
      CHECK(pinPage(bm, h, i));
      sprintf(expected, "%s-%i", "Page", h->pageNum);
      ASSERT_EQUALS_STRING(expected, h->data, "reading back dummy page content");
      CHECK(unpinPage(bm,h));
    }

  CHECK(shutdownBufferPool(bm));

  free(expected);
  free(h);
}

void
testReadPage ()
{
  BM_BufferPool *bm = MAKE_POOL();
  BM_PageHandle *h = MAKE_PAGE_HANDLE();
  testName = "Reading a page";

  CHECK(createPageFile("testbuffer.bin"));
  CHECK(initBufferPool(bm, "testbuffer.bin", 3, RS_FIFO, NULL));

  CHECK(pinPage(bm, h, 0));
  CHECK(pinPage(bm, h, 0));
 

  CHECK(markDirty(bm, h));

  CHECK(unpinPage(bm,h));
  CHECK(unpinPage(bm,h));

  CHECK(forcePage(bm, h));

  CHECK(shutdownBufferPool(bm));
  CHECK(destroyPageFile("testbuffer.bin"));

  free(bm);
  free(h);

  TEST_DONE();
}

void
testFIFO ()
{
  // expected results
  const char *poolContents[] = { 
    "[0 0],[-1 0],[-1 0]" , 
    "[0 0],[1 0],[-1 0]", 
    "[0 0],[1 0],[2 0]", 
    "[1 0],[2 0],[3 0]",
    "[2 0],[3 0],[4 0]",
    "[2 0],[3 0],[4 1]",
    "[3 0],[4 1],[5x0]",
    "[4 1],[5x0],[6x0]",//
    "[4 1],[6x0],[0x0]",
    "[4 0],[6x0],[0x0]",
    "[4 0],[6 0],[0 0]"
  };
  const int requests[] = {0,1,2,3,4,4,5,6,0};
  const int numLinRequests = 5;
  const int numChangeRequests = 3;

  int i;
  BM_BufferPool *bm = MAKE_POOL();
  BM_PageHandle *h = MAKE_PAGE_HANDLE();
  testName = "Testing FIFO page replacement";

  CHECK(createPageFile("testbuffer.bin"));

  createDummyPages(bm, 100);

  CHECK(initBufferPool(bm, "testbuffer.bin", 3, RS_FIFO, NULL));

  // reading some pages linearly with direct unpin and no modifications
  for(i = 0; i < numLinRequests; i++)
    {
      pinPage(bm, h, requests[i]);
      unpinPage(bm, h);
      ASSERT_EQUALS_POOL(poolContents[i], bm, "check pool content");
    }

  // pin one page and test remainder
  i = numLinRequests;
  pinPage(bm, h, requests[i]);
  ASSERT_EQUALS_POOL(poolContents[i],bm,"pool content after pin page");

  // read pages and mark them as dirty
  for(i = numLinRequests + 1; i < numLinRequests + numChangeRequests + 1; i++)
    {
      pinPage(bm, h, requests[i]);
      markDirty(bm, h);
      unpinPage(bm, h);
      ASSERT_EQUALS_POOL(poolContents[i], bm, "check pool content");
    }

  // flush buffer pool to disk
  i = numLinRequests + numChangeRequests + 1;
  h->pageNum = 4;
  unpinPage(bm, h);
  ASSERT_EQUALS_POOL(poolContents[i],bm,"unpin last page");
  
  i++;
  forceFlushPool(bm);
  ASSERT_EQUALS_POOL(poolContents[i],bm,"pool content after flush");

  // check number of write IOs
  ASSERT_EQUALS_INT(3, getNumWriteIO(bm), "check number of write I/Os");
  ASSERT_EQUALS_INT(8, getNumReadIO(bm), "check number of read I/Os");

  CHECK(shutdownBufferPool(bm));
  CHECK(destroyPageFile("testbuffer.bin"));

  free(bm);
  free(h);
  TEST_DONE();
}

// test the LRU page replacement strategy
void
testLRU (void)
{
  // expected results
  const char *poolContents[] = { 
    // read first five pages and directly unpin them
    "[0 0],[-1 0],[-1 0],[-1 0],[-1 0]" , 
    "[1 0],[0 0],[-1 0],[-1 0],[-1 0]",
    "[2 0],[1 0],[0 0],[-1 0],[-1 0]",
    "[3 0],[2 0],[1 0],[0 0],[-1 0]",
    "[4 0],[3 0],[2 0],[1 0],[0 0]",
    // use some of the page to create a fixed LRU order without changing pool content
    "[3 0],[4 0],[2 0],[1 0],[0 0]",
    "[4 0],[3 0],[2 0],[1 0],[0 0]",
    "[0 0],[4 0],[3 0],[2 0],[1 0]",
    "[2 0],[0 0],[4 0],[3 0],[1 0]",
    "[1 0],[2 0],[0 0],[4 0],[3 0]",
    // check that pages get evicted in LRU order
    "[5 0],[1 0],[2 0],[0 0],[4 0]",
    "[6 0],[5 0],[1 0],[2 0],[0 0]",
    "[7 0],[6 0],[5 0],[1 0],[2 0]",
    "[8 0],[7 0],[6 0],[5 0],[1 0]",
    "[9 0],[8 0],[7 0],[6 0],[5 0]"
  };
  const int orderRequests[] = {3,4,0,2,1};
  const int numLRUOrderChange = 5;

  int i;
  int snapshot = 0;
  BM_BufferPool *bm = MAKE_POOL();
  BM_PageHandle *h = MAKE_PAGE_HANDLE();
  testName = "Testing LRU page replacement";

  CHECK(createPageFile("testbuffer.bin"));
  createDummyPages(bm, 100);
  CHECK(initBufferPool(bm, "testbuffer.bin", 5, RS_LRU, NULL));

  // reading first five pages linearly with direct unpin and no modifications
  for(i = 0; i < 5; i++)
  {
      pinPage(bm, h, i);
      unpinPage(bm, h);
      ASSERT_EQUALS_POOL(poolContents[snapshot++], bm, "check pool content reading in pages");
  }

  // read pages to change LRU order
  for(i = 0; i < numLRUOrderChange; i++)
  {
      pinPage(bm, h, orderRequests[i]);
      unpinPage(bm, h);
      ASSERT_EQUALS_POOL(poolContents[snapshot++], bm, "check pool content using pages");
  }

  // replace pages and check that it happens in LRU order
  for(i = 0; i < 5; i++)
  {
      pinPage(bm, h, 5 + i);
      unpinPage(bm, h);
      ASSERT_EQUALS_POOL(poolContents[snapshot++], bm, "check pool content using pages");
  }

  // check number of write IOs
  ASSERT_EQUALS_INT(0, getNumWriteIO(bm), "check number of write I/Os");
  ASSERT_EQUALS_INT(10, getNumReadIO(bm), "check number of read I/Os");

  CHECK(shutdownBufferPool(bm));
  CHECK(destroyPageFile("testbuffer.bin"));

  free(bm);
  free(h);
  TEST_DONE();
}
