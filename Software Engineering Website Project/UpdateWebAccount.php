<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Recharge</title>
</head>
<body>
<h1>Recharge</h1>
<?php
 include 'connection.php';
// Create connection to Oracle
 $conn = connect();
 
if (!$conn) {
   $m = oci_error();
   echo $m['message'], "\n";
   exit;
}
else{
	
	
	session_start();
	$user=$_SESSION['user'];
	$balance=$_POST['recharge'];
	$cardnum=$_POST['card'];

    $sql="select max(r_id) maxid from recharge";
	$maxid=oci_parse($conn,$sql);
	oci_execute($maxid);
	if($row=oci_fetch_array($maxid,OCI_ASSOC))
	   $r_id=$row['MAXID']+1;
	 else
	 $r_id=1;
	 
	 
	date_default_timezone_set('America/Chicago');
    $date=date('m-d-Y');

	//updating the Recharge table
	$sql="INSERT INTO RECHARGE VALUES(".$r_id.",".$cardnum.",'".$user."',"
	."to_date('".$date."','MM-DD-YYYY'),".$balance.")";
	$stmt1=oci_parse($conn,$sql);
	
	oci_execute($stmt1);
        	
	//updating the WEB_ACCOUNT with the recharged amount	
	$sql1="UPDATE WEB_ACCOUNT SET BALANCE=BALANCE+".$balance." WHERE USERNAME='".$user."'";
 	$stmt=oci_parse($conn,$sql1);
	oci_execute($stmt);
        
	echo "Recharged successful";	
	echo "<br>Click <a href=\"Profile.php\">here<a> to go back.";
}
?>
</body>
</html>
